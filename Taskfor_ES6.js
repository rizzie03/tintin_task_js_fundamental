// 1. Buat variable yang isinya array. Namanya bebas kalo bisa sesuaikan konteks. Di dalam array minimal ada 5 data.
// 2. Buat variable yang isinya object. Nama variablenya bebas kalo bisa sesuaikan konteks. Di dalam object minimal ada 4 property. 1 property nilainya harus object lagi <- dalam object tersebut bebas mau di kasi property apa, tapi kalo bisa sesuaikan konteks
// 3. Desctructuring ke 2 variable tersebut. Terus hasil desctructnya tampilin di console ya men-temen
// 4. Buat satu console.log yang nemampilkan data string sama expressi ( variable aja ) pakek template literals ya
// 5. Buat satu variable juga yang dalamnya ada pengkondisian pakek ternary operator. Kemudian hasilnya boleh tampilin lewat console.log
// 6. Buat variable yang isinya array of object. minimal ada 5 data object
// 7. Terus variable array yang no 6. boleh di looping pakek method map dan hasil loopinganya tampilin di console ya guys.
// 8. Masi berhubungan sama variable yang di no 6, silakan buat variable yang isinya hasil dari eksekusi method filter ya guys
// 9. Masi berhubungan sama variable yang di no 6 juga, silakan buat variable yang isinya hasil dari eksekusi method find ya guys.

// Buat di dalam 1 file js aja ya men-temen. Jan lupa di upload di repo gitlab masing-masing. Terus linknya boleh di submit yaww

// no. 1
const Games = [
    "God of War",
    " Resident Evil 4",
    " Dota 2",
    " Horizon Zero Dawn",
    " Nier Automata",
    " Mass Effect Series",
  ];
  
  // no. 2
  const Character = {
    name: "Leon S Kennedy",
    genre: "Horror",
    power: 30,
    role: {
      ranged: "The Silver Ghost",
      melee: "Knive",
    },
  };
  
  // no. 3
  const { name, genre, power, role } = Character;
  console.log(name, genre, power, role);
  const { melee, ranged } = role;
  console.log(melee, ranged);
  const [one, two, ...rest] = Games;
  console.log(one, two, ...rest);
  
  // no. 4
  console.log(`This ${Games} is some of my favorite games of all times`);
  
  // no. 5
  const hero = true;
  const protagonist =
    hero === true ? "He's not an antagonist" : "He's an antagonist";
  console.log(protagonist);
  
  // no. 6
  const Weapons = [
    { name: "Leviathan Axe", power: 1000 },
    { name: "The Silver Ghost", power: 100 },
    { name: "Corrupted Dragon Claw Hook", power: 550 },
    { name: "Banuk Striker", power: 250 },
    { name: "Virtuous Contract", power: 300 },
    { name: "Black Widow", power: 450 },
  ];
  
  // no. 7
  Weapons.map((wep) => {
    console.log(`${wep.name} - ${wep.power + 100}`);
  });
  
  // no. 8
  const mainweapon = Weapons.filter((mainwep) => mainwep.power >= 300);
  console.log(mainweapon);
  
  // no. 9
  const findmainweapon = Weapons.find((findmainwep) => findmainwep.power >= 500);
  console.log(findmainweapon);
  